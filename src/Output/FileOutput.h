#ifndef ECHO_OUTPUT_FILEOUTPUT_H
#define ECHO_OUTPUT_FILEOUTPUT_H

#include "Output.h"
#include "utils.h"

class FileOutput : public Output
{
public:
	FileOutput(int sample_rate, std::string & filename, std::map<std::string, std::string> meta);
	virtual ~FileOutput();

	virtual void put(float * buffer, size_t length);
private:
	void init_format_context(std::string & filename, int sample_rate);
	void init_stream(AVCodecID codec_id, int sample_rate);
	void init_frame();
	int write_packet();

	static int default_buffer_size;

	AVFormatContext * fmtctx;
	AVStream * stream;
	AVFrame * frame;
	float * samples;
	size_t data_i;
	size_t buffer_size;
	size_t frame_size;
};

#endif