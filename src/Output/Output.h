#ifndef ECHO_OUTPUT_OUTPUT_H
#define ECHO_OUTPUT_OUTPUT_H

#include "utils.h"

class Output
{
public:
	Output();

	Output(std::map<std::string, std::string> meta);

	virtual ~Output() { }

	virtual void put(float * buffer, size_t length) = 0;
protected:
	int frequency;

	std::map<std::string, std::string> meta;
};

#endif