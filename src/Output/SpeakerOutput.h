#ifndef ECHO_OUTPUT_SPEAKEROUTPUT_H
#define ECHO_OUTPUT_SPEAKEROUTPUT_H

#include "Output.h"
#include "utils.h"

class SpeakerOutput : public Output
{
public:
	SpeakerOutput(int sample_rate, size_t buffer_size = 16 * 1024 / sizeof(float), int spinwait_time_ms = 10);
	virtual ~SpeakerOutput();

	virtual void put(float * buffer, size_t length);
private:
	SDL_AudioDeviceID dev;

	int sample_rate;

	float * buffer;
	size_t buffer_size;
	size_t buffer_play_index;
	size_t buffer_stashed_length;

	int spinwait_time_ms;
	std::mutex buffer_lock;


	static void callback(void * userdata, Uint8 * stream, int len);
};

#endif