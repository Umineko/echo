#ifndef ECHO_ENCODER_DUMMYENCODER_H
#define ECHO_ENCODER_DUMMYENCODER_H

#include "utils.h"
#include "Encoder.h"

class DummyEncoder : public Encoder
{
public:
	DummyEncoder(Source * source, Output * output, EncoderSettings settings) : Encoder(source, output, settings) { }

	virtual ~DummyEncoder() { }
protected:
	virtual void encode_data(std::vector<float> & buffer) { }
};

#endif