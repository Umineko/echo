#include "SpectrumEncoder.h"

double SpectrumEncoder::encoding_space_fraction = 0.1;
int SpectrumEncoder::base_freq = 800;
int SpectrumEncoder::freq_gap = 10;
double SpectrumEncoder::pick_value = 0.03 / 8;
double SpectrumEncoder::mute_rate = 1;

SpectrumEncoder::SpectrumEncoder(Source * source, Output * output, EncoderSettings settings) : Encoder(source, output, settings) {
	samples_rate = source->get_sample_rate();
	window_size = settings.chunk_size;
	in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * window_size);
	out  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * window_size);
}

SpectrumEncoder::~SpectrumEncoder() {
	fftw_free(in);
	fftw_free(out);
}

char SpectrumEncoder::get_next_symbol() {
	message_locker.lock();
	char data = *message_iterator;
	++message_iterator;
	if (message_iterator == message.end())
		message_iterator = message.begin();
	message_locker.unlock();
	return data;
}

void SpectrumEncoder::encode_data(std::vector<float> & buffer) {
	char data = get_next_symbol();
	buffer.resize(window_size, 0);
	encode_data(buffer, 0, window_size, data);
}

void SpectrumEncoder::encode_data(std::vector<float> & buffer, int offset, int length, char data) {
	for (int i = 0; i < length; ++i) {
		in[i][0] = buffer[i + offset];
		in[i][1] = 0;
	}
	plan = fftw_plan_dft_1d(length, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);

	int cur_freq = base_freq;
	for (int i = cur_freq - 1; i < length / 2; ++i) {
		out[i][0] = out[length - i - 1][0] = out[i][0] * mute_rate;
		out[i][1] = out[length - i - 1][1] = out[i][1] * mute_rate;
	}

	double angle = Pi / 6;
	for (int j = 0; j < 8; ++j, cur_freq += freq_gap) {
		if (data & (1 << j)) {
			out[cur_freq][0] = out[length - cur_freq - 1][0] = pick_value * length * std::sin(angle);
			out[cur_freq][1] = out[length - cur_freq - 1][1] = -pick_value * length * std::cos(angle);
		}
	}

	plan = fftw_plan_dft_1d(length, out, in, FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);

	for (int i = 0; i < length; ++i) {
		buffer[i + offset] = in[i][0] / length;
	}
}