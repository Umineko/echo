#ifndef ECHO_ENCODER_ENCODER_H
#define ECHO_ENCODER_ENCODER_H

#include "utils.h"
#include "Source/Source.h"
#include "Output/Output.h"

struct EncoderSettings
{
	int chunk_size;
};

class Encoder
{
public:
	Encoder(Source * source, Output * output, EncoderSettings settings);

	virtual ~Encoder();

	void set_message(std::string message);

	void start();

	void stop();

	bool is_working();

protected:
	EncoderSettings settings;

	Source * source;
	Output * output;

	std::string message;
	std::string::iterator message_iterator;

	virtual void encode_data(std::vector<float> & buffer) = 0;
	virtual void working_thread_func();

	std::mutex message_locker;
	std::thread working_thread;

	std::mutex working_thread_locker;
	bool working;
};

#endif