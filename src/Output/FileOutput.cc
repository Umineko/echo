#include "FileOutput.h"

int FileOutput::default_buffer_size = 16 * 1024;

FileOutput::FileOutput(int sample_rate, std::string & filename, std::map<std::string, std::string> meta) : Output(meta) {
    init_format_context(filename, sample_rate);
    init_frame();
    data_i = 0;
}

FileOutput::~FileOutput() {
    av_frame_free(&frame);
    while (write_packet());
    av_write_trailer(fmtctx);
    avcodec_close(stream->codec);
    for (int i = 0; i < fmtctx->nb_streams; i++) {
        av_freep(&fmtctx->streams[i]->codec);
        av_freep(&fmtctx->streams[i]);
    }
    if (!(fmtctx->oformat->flags & AVFMT_NOFILE))
        avio_close(fmtctx->pb);
    av_free(fmtctx);
    av_free(samples);
}

void FileOutput::init_format_context(std::string & filename, int sample_rate) {
    fmtctx = avformat_alloc_context();
    auto fmt = av_guess_format(nullptr, filename.c_str(), nullptr);
    if (!fmt) {
        std::cerr << "Filed to deduce format" << std::endl;
        exit(1);
    }

    fmtctx->oformat = fmt;
    snprintf(fmtctx->filename, sizeof(fmtctx->filename), "%s", filename.c_str());

    init_stream(fmt->audio_codec, sample_rate);

    for (auto j = meta.begin(); j != meta.end(); ++j) {
        av_dict_set(&fmtctx->metadata, (*j).first.c_str(), (*j).second.c_str(), 0);
        //av_dict_set(&stream->metadata, (*j).first.c_str(), (*j).second.c_str(), 0);
    }

#ifdef DEBUG
    av_dump_format(fmtctx, 0, filename.c_str(), 1);
#endif

    if (!(fmt->flags & AVFMT_NOFILE)) {
        if (avio_open(&fmtctx->pb, filename.c_str(), AVIO_FLAG_WRITE) < 0) {
            std::cerr << "Could not open " << filename << std::endl;
            exit(1);
        }
    }

    avformat_write_header(fmtctx, nullptr);
}

void FileOutput::init_stream(AVCodecID codec_id, int sample_rate) {
    auto codec = avcodec_find_encoder(codec_id);
    if (!codec) {
        std::cerr << "No codec found" << std::endl;
        exit(1);
    }
    stream = avformat_new_stream(fmtctx, codec);
    if (!stream) {
        std::cerr << "Could not alloc stream" << std::endl;
        exit(1);
    }
    auto ctx = stream->codec;

    ctx->bit_rate       = 192 * 1000;
    ctx->sample_fmt     = AV_SAMPLE_FMT_FLTP;
    ctx->sample_rate    = sample_rate;
    ctx->channel_layout = AV_CH_LAYOUT_MONO;
    ctx->channels       = av_get_channel_layout_nb_channels(ctx->channel_layout);\

    if (fmtctx->oformat->flags & AVFMT_GLOBALHEADER)
        ctx->flags |= CODEC_FLAG_GLOBAL_HEADER;

#ifdef DEBUG
    const enum AVSampleFormat *p = codec->sample_fmts;
    bool has_sample_fmt = false;
    std::cout << "Has sample formats:" << std::endl;
    while (*p != AV_SAMPLE_FMT_NONE) {
        std::cout << *p << std::endl;
        if (*p == ctx->sample_fmt) 
            has_sample_fmt = true;
        p++;
    }
#endif

    if (avcodec_open2(ctx, codec, nullptr) < 0) {
        std::cerr << "could not open codec" << std::endl;
        exit(1);
    }
}

void FileOutput::init_frame() {
    auto ctx = stream->codec;

    frame = av_frame_alloc();
    frame->nb_samples     = ctx->frame_size;
    frame->format         = ctx->sample_fmt;
    frame->channel_layout = ctx->channel_layout;

    buffer_size = ctx->frame_size 
        ? av_samples_get_buffer_size(nullptr, ctx->channels, ctx->frame_size, ctx->sample_fmt, 0)
        : default_buffer_size;
    frame_size = ctx->frame_size ? ctx->frame_size : default_buffer_size / sizeof(float);
    samples = (float *)av_malloc(buffer_size);
    avcodec_fill_audio_frame(frame, ctx->channels, ctx->sample_fmt, (const uint8_t *)samples, buffer_size, 0);
}

int FileOutput::write_packet() {
    AVPacket pkt;
    int got_output;
    av_init_packet(&pkt);
    pkt.data = nullptr;
    pkt.size = 0;
    int errcode = avcodec_encode_audio2(stream->codec, &pkt, frame, &got_output);
    if (errcode) {
        std::cerr << "Failed to encode. Error #" << errcode << std::endl;
        exit(1);
    }
    if (got_output) {
        if (av_interleaved_write_frame(fmtctx, &pkt) != 0) {
            std::cerr << "Error while writing audio frame" << std::endl;
            exit(1);
        }
    }
    av_free_packet(&pkt);
    return got_output;
}

void FileOutput::put(float * buffer, size_t length) {
    for (int i = 0; i < length; ++i) {
        if (data_i == frame_size) {
            write_packet();
            data_i = 0;
        }
        samples[stream->codec->channels * data_i] = buffer[i];
        for (int j = 0; j < stream->codec->channels; ++j)
            samples[stream->codec->channels * data_i + j] = samples[stream->codec->channels * data_i];
        ++data_i;
    }
}