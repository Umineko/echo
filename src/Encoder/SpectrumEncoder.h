#ifndef ECHO_ENCODER_SPECTRUMENCODER_H
#define ECHO_ENCODER_SPECTRUMENCODER_H

#include "utils.h"
#include "Encoder.h"

class SpectrumEncoder : public Encoder
{
public:
	SpectrumEncoder(Source * source, Output * output, EncoderSettings settings);

	virtual ~SpectrumEncoder();
protected:
	virtual void encode_data(std::vector<float> & buffer);

private:
	static double encoding_space_fraction;
	static int base_freq;
	static int freq_gap;
	static double pick_value;
	static double mute_rate;
	virtual void encode_data(std::vector<float> & buffer, int offset, int length, char data);
	virtual char get_next_symbol();

	size_t samples_rate;
	size_t window_size;

	fftw_complex *in;
	fftw_complex *out;
	fftw_plan plan;
};

#endif