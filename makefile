PROJECTPATH = $(shell readlink -m .)
SRCPATH = $(shell readlink -m ./src)
OUTPATH = $(shell readlink -m ./bin)

SDL_LIB = -L/usr/local/lib -lSDL2 -Wl,-rpath=/usr/local/lib
EFLAGS = -pthread -lfftw3 -lavformat -lavcodec -lavfilter -lavutil -lmp3lame -lz -lva -lbz2 -lcrypto++ -std=c++11 -I$(SRCPATH) $(SDL_LIB)
OFLAGS = -c $(EFLAGS)

OBJS = $(OUTPATH)/FileSource.o $(OUTPATH)/SpeakerOutput.o $(OUTPATH)/DummyEncoder.o

all: echo

debug: CXX += -DDEBUG -g
debug: echo

echo:
	$(CXX) $(SRCPATH)/main.cc \
		$(SRCPATH)/Source/FileSource.cc \
		$(SRCPATH)/Output/Output.cc \
		$(SRCPATH)/Output/SpeakerOutput.cc \
		$(SRCPATH)/Output/FileOutput.cc \
		$(SRCPATH)/Encoder/Encoder.cc \
		$(SRCPATH)/Encoder/SpectrumEncoder.cc \
		$(SRCPATH)/utils.cc \
		$(EFLAGS) -o $(OUTPATH)/$@

test:
	$(CXX) $(SRCPATH)/test.cc \
		$(SRCPATH)/utils.cc \
		$(EFLAGS) -o $(OUTPATH)/$@

clean:
	rm $(OUTPATH)/*
