#include "SpeakerOutput.h"

SpeakerOutput::SpeakerOutput(int sample_rate, size_t buffer_size, int spinwait_time_ms) 
	: sample_rate(sample_rate), buffer_size(buffer_size), spinwait_time_ms(spinwait_time_ms) {
	buffer = new float[buffer_size];
	buffer_play_index = 0;
	buffer_stashed_length = 0;
	
	if (SDL_Init(SDL_INIT_AUDIO) != 0){
		std::ostringstream msg;
		msg << "SDL_Init Error: " << SDL_GetError();
		die(msg.str());
	}

	SDL_AudioSpec want, have;

	SDL_zero(want);
	want.freq = sample_rate;
	want.format = AUDIO_F32;
	want.channels = 1;
	want.samples = 16 * 1024;
	want.userdata = this;
	want.callback = SpeakerOutput::callback;

	dev = SDL_OpenAudioDevice(nullptr, false, &want, &have, SDL_AUDIO_ALLOW_FORMAT_CHANGE);

	if (dev == 0 || have.format != AUDIO_F32) {
		std::ostringstream msg;
		msg << "Failed to open audio: " << SDL_GetError();
		die(msg.str());
	}

    SDL_PauseAudioDevice(dev, 0);
}

SpeakerOutput::~SpeakerOutput() {
    SDL_CloseAudioDevice(dev);
	SDL_Quit();
}

void SpeakerOutput::put(float * input_buffer, size_t length) {
	size_t copied = 0;
	while (length) {
		buffer_lock.lock();
		size_t to_copy = std::min(buffer_size - buffer_stashed_length, length);
		size_t start_index = (buffer_play_index + buffer_stashed_length) % buffer_size;
		for (size_t i = 0; i < to_copy; ++i) {
			buffer[(start_index + i) % buffer_size] = input_buffer[copied++];
		}
		length -= to_copy;
		buffer_stashed_length += to_copy;
		buffer_lock.unlock();
		if (length) {
			std::this_thread::sleep_for(std::chrono::milliseconds(spinwait_time_ms));
		}
	}
}

void SpeakerOutput::callback(void * userdata, Uint8 * stream, int len) {
	auto out = (float *)stream;
	len /= sizeof(float);
	auto speaker = (SpeakerOutput *)userdata;
	size_t copied = 0;
	while (len) {
		speaker->buffer_lock.lock();
		size_t to_copy = std::min(speaker->buffer_stashed_length, (size_t)len);
		for (size_t i = 0; i < to_copy; ++i) {
			out[copied++] = speaker->buffer[(speaker->buffer_play_index + i) % speaker->buffer_size];
		}
		len -= to_copy;
		speaker->buffer_play_index = (speaker->buffer_play_index + to_copy) % speaker->buffer_size;
		speaker->buffer_stashed_length -= to_copy;
		speaker->buffer_lock.unlock();
		if (len) {
			std::this_thread::sleep_for(std::chrono::milliseconds(speaker->spinwait_time_ms));
		}
	}
}