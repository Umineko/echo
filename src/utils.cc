#include "utils.h"

void die(const std::string & message) {
	std::cerr << message << std::endl;
	exit(1);
}