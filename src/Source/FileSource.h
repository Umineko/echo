#ifndef ECHO_SOURCE_FILESOURCE_H
#define ECHO_SOURCE_FILESOURCE_H

#include "utils.h"
#include "Source.h"

class FileSource : public Source {
public:
	virtual ~FileSource();

	FileSource(std::string & filename);

	virtual size_t get(float * out_buffer, size_t length);

	virtual int get_sample_rate();

	std::string get_title();

	virtual std::map<std::string, std::string> get_meta();
private:
	void ensure_buffer_capacity(size_t length);
	void convert_frame_data(AVFrame * frame, std::vector<float> & buf);

	std::vector<float>  buffer;
	AVFormatContext * container;
	AVCodecContext * ctx;
	AVCodec * codec;
	int used_stream;

	std::string title;
};

#endif