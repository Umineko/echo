#include "Encoder.h"

Encoder::Encoder(Source * source, Output * output, EncoderSettings settings) : source(source), output(output), settings(settings), working(false) {

}

Encoder::~Encoder() {
	working = false;
	working_thread.join();
}

void Encoder::set_message(std::string message) {
	message_locker.lock();
	this->message.resize(message.size());
	std::copy(message.begin(), message.end(), this->message.begin());
	message_iterator = this->message.begin();
	message_locker.unlock();
}

void Encoder::start() {
	working_thread_locker.lock();
	working_thread = std::thread(&Encoder::working_thread_func, this);
	working = true;
	working_thread_locker.unlock();
}

void Encoder::stop() {
	if (!working)
		return;
	working = false;
}

void Encoder::working_thread_func() {
	while (working) {
		std::vector<float> buffer(settings.chunk_size, 0);
		int length = source->get(&*buffer.begin(), settings.chunk_size);
		buffer.resize(length);
		if (length == 0) {
			std::cout << "Got zero length packet" << std::endl;
			stop();
		}
		encode_data(buffer);
		output->put(&*buffer.begin(), buffer.size());
	}
}

bool Encoder::is_working() {
	return working;
}