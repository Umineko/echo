#ifndef ECHO_UTILS_H
#define ECHO_UTILS_H

#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <vector>
#include <map>
#include <chrono>
#include <sstream>
#include <fstream>
#include <cmath>
#include <ctime>

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>
#include <cryptopp/hex.h>


extern "C" {

#include "fftw3.h"
#include "libavcodec/avcodec.h"
#include "libavutil/channel_layout.h"
#include "libavformat/avformat.h"
#include "libavutil/opt.h"
#include "libavutil/common.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "ao/ao.h"
#include "portaudio.h"
#include "SDL2/SDL.h"

}


void die(const std::string & message);

constexpr double Pi = std::asin(1.0) * 2;
const double Eps = 1e-5;

#endif