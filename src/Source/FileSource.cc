#include "FileSource.h"


FileSource::~FileSource() {
    avcodec_close(ctx);
    avformat_close_input(&container);
}

FileSource::FileSource(std::string & filename) {
    av_register_all();
	container = avformat_alloc_context();
    if (avformat_open_input(&container, filename.c_str(), nullptr, nullptr) < 0) {
        die("Cannot open file");
    }

    if (avformat_find_stream_info(container, nullptr) < 0) {
        die("Cannot find stream info");
    }
#ifdef DEBUG
    av_dump_format(container, 0, filename.c_str(), false);
#endif

    AVDictionaryEntry *tag = nullptr;
    while ((tag = av_dict_get(container->metadata, "", tag, AV_DICT_IGNORE_SUFFIX))) {
#ifdef DEBUG
        std::cout << "Key: " << tag->key << " Value: " << tag->value << std::endl;
#endif
        meta[std::string(tag->key)] = tag->value;
        if (std::string(tag->key) == "title")
            title = std::string(tag->value);
    }

    used_stream = -1;
    for (int i = 0; i < container->nb_streams; i++) {
        if (container->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
            used_stream = i;
            break;
        }
    }

    if (used_stream == -1) {
        die("Cannot find any audio stream");
    }

    AVDictionary * metadata = container->metadata;

    ctx = container->streams[used_stream]->codec;
    codec = avcodec_find_decoder(ctx->codec_id);

    if (codec == nullptr || avcodec_open2(ctx, codec, nullptr) < 0) {
        die("Cannot open codec");
    }
}

size_t FileSource::get(float * out_buffer, size_t length) {
	ensure_buffer_capacity(length);
	length = std::min(buffer.size(), length);
	std::copy(buffer.begin(), buffer.begin() + length, out_buffer);
	std::copy(buffer.begin() + length, buffer.end(), buffer.begin());
	buffer.resize(buffer.size() - length);
	return length;
}

void FileSource::ensure_buffer_capacity(size_t length) {
    AVPacket packet;
    av_init_packet(&packet);
    AVFrame * frame = av_frame_alloc();

    while (buffer.size() < length && av_read_frame(container, &packet) >= 0) {
    	if (packet.stream_index != used_stream)
    		continue;

    	int got_frame;
        avcodec_decode_audio4(ctx, frame, &got_frame, &packet);
        if (!got_frame) {
        	die("Failed to get frame");
        }

        std::vector<float> buf;
        convert_frame_data(frame, buf);
        size_t old_size = buffer.size();
        buffer.resize(old_size + buf.size());
        std::copy(buf.begin(), buf.end(), buffer.begin() + old_size);

    	av_free_packet(&packet);
    }

    av_frame_free(&frame);
}

void FileSource::convert_frame_data(AVFrame * frame, std::vector<float> & buf) {
	int plane_size;
    av_samples_get_buffer_size(&plane_size, ctx->channels, frame->nb_samples, ctx->sample_fmt, 1);
    buf.clear();
    size_t samples_count;
    switch (ctx->sample_fmt) {
        case AV_SAMPLE_FMT_S16:
        case AV_SAMPLE_FMT_S16P:
        	samples_count = plane_size / sizeof(short);
        	buf.resize(samples_count);
            for (size_t i = 0; i < samples_count; ++i) {
            	float value = 0;
                for (size_t ch = 0; ch < ctx->channels; ch++) {
                    value += ((short *) frame->extended_data[ch])[i];
                }
                buf[i] = value / ctx->channels / std::numeric_limits<short>::max();
            }
            break;
        case AV_SAMPLE_FMT_U8:
        case AV_SAMPLE_FMT_U8P:
        	samples_count = plane_size / sizeof(unsigned char);
        	buf.resize(samples_count);
            for (size_t i = 0; i < samples_count; ++i) {
            	float value = 0;
                for (size_t ch = 0; ch < ctx->channels; ch++) {
                    value += ((unsigned char *) frame->extended_data[ch])[i];
                }
                buf[i] = ((value / ctx->channels) - std::numeric_limits<char>::max())
                	/ std::numeric_limits<char>::max();
            }
            break;
        case AV_SAMPLE_FMT_FLT:
        case AV_SAMPLE_FMT_FLTP:
        	samples_count = plane_size / sizeof(float);
        	buf.resize(samples_count);
            for (size_t i = 0; i < samples_count; ++i) {
            	float value = 0;
                for (size_t ch = 0; ch < ctx->channels; ch++) {
                    value += ((float *) frame->extended_data[ch])[i];
                }
                buf[i] = value / ctx->channels;
            }
            break;
        default:
            die("Unknown sample format");
    }
}

int FileSource::get_sample_rate() {
    return ctx->sample_rate;
}

std::string FileSource::get_title() {
    return title;
}

std::map<std::string, std::string> FileSource::get_meta() {
    return meta;
}