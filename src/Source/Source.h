#ifndef ECHO_SOURCE_SOURCE_H
#define ECHO_SOURCE_SOURCE_H

#include "utils.h"

class Source {
public:
	virtual ~Source() {}

	virtual size_t get(float * out_buffer, size_t length) = 0;

	virtual int get_sample_rate() = 0;

	virtual std::map<std::string, std::string> get_meta() {
		return meta;
	}

protected:
	std::map<std::string, std::string> meta;
};

#endif