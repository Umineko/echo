#include "utils.h"
#include "Source/FileSource.h"
#include "Output/SpeakerOutput.h"
#include "Output/FileOutput.h"
#include "Encoder/DummyEncoder.h"
#include "Encoder/SpectrumEncoder.h"

#pragma comment(linker, "/stack:268435456")

const int message_ttl_seconds = 1;

std::string calculate_flag(std::string seed) {
	CryptoPP::Weak1::MD5 hash;
	byte digest[ CryptoPP::Weak1::MD5::DIGESTSIZE ];

	hash.CalculateDigest( digest, (byte*) seed.c_str(), seed.length() );

	CryptoPP::HexEncoder encoder;
	std::string output;
	encoder.Attach( new CryptoPP::StringSink( output ) );
	encoder.Put( digest, sizeof(digest) );
	encoder.MessageEnd();

	return "Congratulations! You deserve 500 points for this! RUCTF_" + output + "\xff";
}

int main(int argc, char ** argv) 
{
	if (argc < 3) {
		std::cout << "Use syntax echo <in_file> <out_file>" << std::endl;
		exit(0);
	}
	std::string filename     = argv[1];
	std::string out_filename = argv[2];
	EncoderSettings settings { 2 * 1024 };

	auto source     = new FileSource(filename);
	int sample_rate = source->get_sample_rate();
	auto output     = out_filename == "-" 
		? (Output *)(new SpeakerOutput(sample_rate)) 
		: (Output *)(new FileOutput(sample_rate, out_filename, source->get_meta()));
	auto encoder    = new SpectrumEncoder(source, output, settings);

	auto title = source->get_title();
	if (title == "")
		std::cout << "Warning! Empty title in file " << filename << "!" << std::endl;

	encoder->set_message(calculate_flag(title));

	encoder->start();

	while (encoder->is_working()) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	delete encoder;
	delete source;
	delete output;
}